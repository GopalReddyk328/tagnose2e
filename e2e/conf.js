var HtmlReporter = require('C:/Users/sairam/node_modules/protractor-html-screenshot-reporter');

var reporter=new HtmlReporter({
    baseDirectory: 'F:/jboss-4.2.1.GA/server/all/deploy/phoenixmapp.war/test/e2e/Reports/', // a location to store screen shots.
    docTitle: 'Test-1',
    docName:    'protractor-demo-tests-report.html'
});

exports.config = {
  seleniumAddress: 'http://localhost:5577/wd/hub',


  capabilities: {
	    browserName: 'chrome'
	  },
	  specs: ['ReportsValidation.js'],

	//	maxSessions: 1,                 //Limits max number of instances


  /*multiCapabilities: [{               // For multiple instances/Users login
	    browserName: 'chrome',
	    acceptSslCerts: true,
	    trustAllSSLCertificates: true,
	    specs: ['ReportsValidation.js'],
	    'chromeOptions': {
	        args: ['--test-type']
	    }
	},

	{
	    browserName: 'chrome',
	    acceptSslCerts: true,
	    trustAllSSLCertificates: true,
	    specs: ['ReportsValidation1.js'],
	    'chromeOptions': {
	        args: ['--test-type']
	    }
	}],*/


	onPrepare: function() {
        jasmine.getEnv().addReporter(reporter);
    },

  /*  onPrepare: function () {
		  require('C:/Users/sairam/node_modules/protractor-trx-reporter');
		  jasmine.getEnv().addReporter(
		    new jasmine.TrxReporter('C:/Users/sairam/node_modules/protractor-trx-reporter/Report.trx'));
		},*/

/*abcd    onComplete: function(runner, log) {


/*    onComplete: function(runner, log) {

        console.log('Automation Tests Complete.');
        browser.driver.getSession().then(function(session) {     // Get Session ID here
          console.log("Session ID = " + session.getId());
        });
      },xyz */

  jasmineNodeOpts: {
	    showColors: true,
	    defaultTimeoutInterval: 900000
	  }
};


