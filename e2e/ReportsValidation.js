var ptor = browser;
  
browser.driver.manage().window().maximize();

browser.driver.get('http://localhost:8080/phoenixmapp/app/');
ptor.sleep(2000);

beforeEach(function () {
	browser.ignoreSynchronization = true;
	});


describe('Test-1', function() {	
    
      
     	 it('TESTCASE-1 : Should have a title', function() {    	 
     	     expect(browser.driver.getTitle()).toContain('data:,');
     	    
     	  
     	  });
     	
         it('TESTCASE-2 : Should accept a valid email address and password', function() {
        	 ptor.sleep(2000);
         	var emailclass = element.all(by.css('.form-group')).get(0);
         	var pwdclass = element.all(by.css('.form-group')).get(1);
         	var btnclass = element.all(by.css('.form-group')).get(2);
         	
 		    var email = emailclass.element(by.id('email'));
 		    var password = pwdclass.element(by.id('password'));
 		    var button = btnclass.element(by.css('.btn')); 		    
 		   ptor.sleep(1000);   
     	    email.sendKeys('superadmin');
    	 
     	   password.sendKeys('12345678');
    	    ptor.sleep(800);      	    
    	    button.click();
    	    ptor.sleep(2000); 
            expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/home');
       
            ptor.sleep(2000); 
            
     	  });
     	 
     	
			
		it('TESTCASE-3: Dashboard Selection.....', function(){ 
			
			 var dropdown = element.all(by.css('.dropdown')).get(1);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(0);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	
	       
	       	   expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/1');
	       	ptor.sleep(2000);
        });
		
		it('TESTCASE-4 : ER Dashboard Add Patient',function(){
       	 ptor.sleep(2000);
        	 var addButton= element.all(by.id('add')).get(0);
    	     addButton.click();  
    	     ptor.sleep(3000); 
 			 element(by.model('newrecord.firstName')).sendKeys('catlin');
 			 ptor.sleep(1000);
 	 	     element(by.model('newrecord.lastName')).sendKeys('nick');
 	 	     element(by.model('newrecord.sex')).sendKeys('M');
 	 	     var Sex = element(by.css('[ng-click="selectMatch($index)"]'));
 	 	     Sex.click();
 	 	     element(by.model('newrecord.title')).sendKeys('Mr');
 	 	     ptor.sleep(1000);  	 	     
 	         var datepic = element.all(by.model('newrecord.dob')).sendKeys('07/22/1990');
            datepic.click();
            element(by.model('newrecord.language')).sendKeys('English');
            element(by.model('newrecord.alias')).sendKeys('Rick');
            ptor.sleep(3000);
        	 element( by.css('[ng-click="ok()"]') ).click();
 	     	
 	     	  ptor.sleep(3000);  
 	     	  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/1');
    	    });
		
      it('TESTCASE-5 - ER Dashboard Add Appointment',function (){	    	
   	     ptor.sleep(2000);
        	 var addButton= element.all(by.id('add')).get(1);
 	    	 addButton.click();  
 	    	 ptor.sleep(3000);
 	    	 var patname = element.all(by.model('newrecord.patientId'));
 	    	 element(by.model('newrecord.patientId')).sendKeys('catlin');
 	    	 ptor.sleep(2000);
 	    	 var pat = element.all(by.css('[ng-click="selectMatch($index)"]')).get(0);
 	    	 pat.click();
 	    	 ptor.sleep(2000);
 	      	    	  
 	    	 var procName = element(by.model('newrecord.procedureId'));
 		  	 var procOptions = procName.all(by.options('procedureId.name as procedureId.value for procedureId in lookups.procedureName'));
 		  	 var procedure = procOptions.get(1);
 		  	 procedure.click();
 		  	 ptor.sleep(3000);
 		  
 		  	 
 		  	 var fon = element.all(by.model('newrecord.phoneNumber')).sendKeys('7854565892');
 		  	 ptor.sleep(1000);
 		  	    
 		  	 		  	    
 		  	  element( by.css('[ng-click="ok()"]') ).click();
 		  	 ptor.sleep(3000);
 		  	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/1');
 		  	ptor.sleep(3000);
      });
      
      it('TESTCASE-6 :  Appointment Status', function(){
        	
      	 var status = element.all(by.model('searchModel.apptOption')).sendKeys('Active');
      	     status.click();
      	     ptor.sleep(1000);
      	 var clear = element.all(by.css('[ng-click="clearSearch()"]'));
      	    clear.click();
      	    ptor.sleep(3000);
      	 var status2 = element.all(by.model('searchModel.apptOption')).sendKeys('All');
      	     status2.click();
      	     ptor.sleep(1000);
      	 var clear = element.all(by.css('[ng-click="clearSearch()"]'));
       	    clear.click();
       	    ptor.sleep(3000);    
       	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/1');   
       });
       
   
       it('TESTCASE-11 : Search Field',function(){
      	
      	
      	 
      	 element.all(by.model('searchModel.searchText')).sendKeys('Flcon');
      	 ptor.sleep(2000);
      	 element(by.css('[ng-click="clearSearch()"]')).click();
      	 ptor.sleep(2000);
      	 element.all(by.model('searchModel.searchText')).sendKeys('06/30/2001');
      	 ptor.sleep(2000);
      	 element(by.css('[ng-click="clearSearch()"]')).click();
      	 element.all(by.model('searchModel.searchText')).sendKeys('tg961');
      	 ptor.sleep(2000);
      	 element(by.css('[ng-click="clearSearch()"]')).click();
      	 ptor.sleep(1000);
      	 element.all(by.model('searchModel.searchText')).sendKeys('7854565892');
      	 ptor.sleep(2000);
      	 element(by.css('[ng-click="clearSearch()"]')).click();
      	 ptor.sleep(1000);
      	 element.all(by.model('searchModel.searchText')).sendKeys('Joseph');
      	 ptor.sleep(2000);
      	 element(by.css('[ng-click="clearSearch()"]')).click();
      	 ptor.sleep(2000);
      	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/1');
       });
       it('ER Dashboard Add comment',function (){
		  	  var comment=element.all(by.css('.imgComment')).get(0);
		  	  comment.click();
		  	 ptor.sleep(2000);
		  	 element.all(by.model('comment')).sendKeys('test');
		  	 element.all(by.css('[ng-click="saveComments();"]')).click();
		  	 ptor.sleep(2000);
	})
	it('ER Dashboard Add phone number',function (){
		  	  var phne=element.all(by.css('.imgComment')).get(1);
		  	  phne.click();
		  	  var phne1=element.all(by.model('listItem.phoneNumber'));
		  	  phne1.sendKeys('9885487520');
		  	  ptor.sleep(2000);
	})
	it('ER Dashboard Add tag',function (){

		var list= element.all(by.css('table')).get(0);
		 var tra=list.all(by.css('tr')).get(1);
		 var tda=tra.all(by.css('td')).get(9);
		 var clk=tda.element(by.css('img'));
		 clk.click();
		  	
		  	ptor.sleep(2000);
		  	element.all(by.model('assignedTagId.tagIntId')).sendKeys('151');
		  	 ptor.sleep(2000);
 	    	 var pat = element.all(by.css('[ng-click="selectMatch($index)"]')).get(0);
 	    	 pat.click();
		
		  	ptor.sleep(5000);
		  	 element.all(by.css('[ng-click="saveTag()"]')).click();
		  	ptor.sleep(2000);
		  	
		  
	})
	it('ER dash board show both panels',function(){
		element.all(by.css('[ng-click="showBothPanels()"]')).click();
		ptor.sleep(5000);
	})
	it('ER dash board show Right panel',function(){
		element.all(by.css('[ng-click="showRightPanel()"]')).click();
		ptor.sleep(5000);
	})
	it('ER dash board show Left panel',function(){
		element.all(by.css('[ng-click="showLeftPanel()"]')).click();
		ptor.sleep(5000);
	})
	
	/*it('ER Dashboard  Complete appointment',function (){
		  	// delete appt
		var table=element.all(by.css('table')).get(0);
	  	var tr=table.all(by.css('tr')).get(1);
	  	var td=tr.all(by.css('td')).get(9);
	  	var img=td.all(by.css('img')).get(0);
	  	img.click();
		  	ptor.sleep(2000);
		  	element.all(by.css('[ng-click="ok()"]')).click();
		  	ptor.sleep(5000);
		  	
  });*/
});	
describe('Test-2  OR Dashboard selections', function() {
	ptor.sleep(2000)
	
    
   it('OR Dashboard Selection.....', function(){ 
	   ptor.sleep(2000)
     	 var dropdown = element.all(by.css('.dropdown')).get(1);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(1);
        		test.click(); 
        	  ptor.sleep(1000);
     	
     	   expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/2');
  ptor.sleep(2000);
 element.all(by.model('searchModel.apptOption')).sendKeys('Active');
 ptor.sleep(2000);
 element.all(by.model('searchModel.apptOption')).sendKeys('All');
 ptor.sleep(2000);
 element.all(by.model('searchModel.searchText')).sendKeys('duminy');
  ptor.sleep(2000);
 element.all(by.model('searchModel.searchText')).clear();
 element.all(by.model('searchModel.modalityOption')).sendKeys('mri');
 ptor.sleep(2000);
 element.all(by.model('searchModel.modalityOption')).clear();
 element.all(by.model('searchModel.providerOption')).sendKeys('David ');
 ptor.sleep(2000);
 element.all(by.model('searchModel.providerOption')).clear();
 element.all(by.model('searchModel.apptOption')).sendKeys('All');
 ptor.sleep(2000);
 
 
   })
})
describe('Test-3:Operations On OR Dashboard', function() {
	
	it('Add Patient in OR.....', function(){ 
		ptor.sleep(2000)
		 var button = element.all(by.css('.btn')).get(0);
     	 button.click();
     	
     	   expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/patientDashboard/encounters/2');
  ptor.sleep(2000);
  var addButton= element.all(by.id('add')).get(0);
  addButton.click();  
  ptor.sleep(3000); 
	 element(by.model('newrecord.firstName')).sendKeys('Leweres');
	 ptor.sleep(1000);
    element(by.model('newrecord.lastName')).sendKeys('Lain');
    element(by.model('newrecord.sex')).sendKeys('M');
    var Sex = element(by.css('[ng-click="selectMatch($index)"]'));
    Sex.click();
    element(by.model('newrecord.title')).sendKeys('Mr');
    ptor.sleep(1000);  	 	     
   var datepic = element.all(by.model('newrecord.dob')).sendKeys('07/22/1990');
 datepic.click();
 element(by.model('newrecord.language')).sendKeys('English');
 element(by.model('newrecord.alias')).sendKeys('Rick');
 ptor.sleep(3000);
 element( by.css('[ng-click="ok()"]') ).click();
	
	  ptor.sleep(2000);  
	  
 
	})
	it('OR Dashboard Add Appointment',function (){	
		ptor.sleep(1000)
		  	 var addButton= element.all(by.id('add')).get(1);
	    	 addButton.click();  
	    	 ptor.sleep(3000);
	    	 var patname = element.all(by.model('newrecord.patientId'));
	    	 element(by.model('newrecord.patientId')).sendKeys('Leweres');
	    	 ptor.sleep(2000);
	    	 var pat = element.all(by.css('[ng-click="selectMatch($index)"]')).get(0);
	    	 pat.click();
	    	 ptor.sleep(2000);
	      	    	  
	    	 var procName = element(by.model('newrecord.procedureId'));
 		  	 var procOptions = procName.all(by.options('procedureId.name as procedureId.value for procedureId in lookups.procedureName'));
 		  	 var procedure = procOptions.get(1);
 		  	 procedure.click();
 		  	 ptor.sleep(3000);	    
			  element( by.css('[ng-click="ok()"]') ).click();
			  
		  	 ptor.sleep(3000);
	})
	it('OR Dashboard Add comment',function (){
		  	  var comment=element.all(by.css('.imgComment')).get(0);
		  	  comment.click();
		  	 ptor.sleep(2000);
		  	 element.all(by.model('comment')).sendKeys('test');
		  	 element.all(by.css('[ng-click="saveComments();"]')).click();
		  	 ptor.sleep(4000);
	})
	
	it('OR Dashboard Add phone number',function (){
		  	  var phne=element.all(by.css('.imgComment')).get(2);
		  	  phne.click();
		  	  var phne1=element.all(by.model('listItem.phoneNumber'));
		  	  phne1.sendKeys('9885487520');
		  	  ptor.sleep(2000);
	})
	it('OR Dashboard Add tag',function (){

		var list= element.all(by.css('table')).get(0);
		 var tra=list.all(by.css('tr')).get(1);
		 var tda=tra.all(by.css('td')).get(11);
		 var clk=tda.element(by.css('img'));
		 clk.click();
		 element.all(by.model('assignedTagId.tagIntId')).sendKeys('161');
	  	 ptor.sleep(2000);
	    	 var pat = element.all(by.css('[ng-click="selectMatch($index)"]')).get(0);
	    	 pat.click();
		  	
		  	ptor.sleep(5000);
		  	 element.all(by.css('[ng-click="saveTag()"]')).click();
		  	ptor.sleep(2000);
		  	
		  
	})
	it('OR dash board show both panels',function(){
		element.all(by.css('[ng-click="showBothPanels()"]')).click();
		ptor.sleep(5000);
	})
	it('OR dash board show Right panel',function(){
		element.all(by.css('[ng-click="showRightPanel()"]')).click();
		ptor.sleep(5000);
	})
	it('OR dash board show Left panel',function(){
		element.all(by.css('[ng-click="showLeftPanel()"]')).click();
		ptor.sleep(5000);
	})
})
describe('Test-3 sorting....', function() {	
	it('sorting.....by patient', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(2);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by account number', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(3);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by MRN', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(4);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by DOB', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(5);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by 	Provider', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(6);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by 	Admit Time', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(7);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by 	Phone Number', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(8);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})
	it('sorting.....by 	Tag Id', function(){
	var sort1=element.all(by.css('.pix2')).get(0);
	var Aname=sort1.all(by.css('td')).get(9);
	var sort=Aname.all(by.css('a')).get(0);
	sort.click();
	ptor.sleep(5000);
	})

})
describe('Test-4 Visitor Dashboard....', function() {
 it('Visitor Dashboard', function (){
     	 
	 var dropdown = element.all(by.css('.dropdown')).get(1);
	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
	 toggle.click();
	  ptor.sleep(1000);
	  
	  var list = dropdown.all(by.css('.dropdown-menu li'));
	  var test= list.get(2);
		test.click(); 
	  ptor.sleep(1000);
     	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/visitorDashboard/encounters/2');
     	 ptor.sleep(2000);
      });
});
describe('Test-5 Tracking Dashboard Operations', function() {
	ptor.sleep(2000);
	
    
   it('Tracking Dashboard serch.....', function(){ 
	   ptor.sleep(2000)
   	 var dropdown = element.all(by.css('.dropdown')).get(2);
      	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
      	 toggle.click();
      	  ptor.sleep(1000);
      	  
      	  var list = dropdown.all(by.css('.dropdown-menu li'));
      	  var test= list.get(0);
      		test.click(); 
      	  ptor.sleep(1000);

   	 element.all(by.model('searchModel.floor')).sendKeys('Floor2');
   	ptor.sleep(5000);
   	element.all(by.model('searchModel.floor')).sendKeys('Floor3');
   	ptor.sleep(5000);
 	element.all(by.model('searchModel.floor')).sendKeys('Floor4');
   	ptor.sleep(5000);
   	
   	var zone=element.all(by.model('searchModel.tagZone')).sendKeys('4-RM#412');
	ptor.sleep(5000);
   	var Zneselect=zone.all(by.options('[zonesInFloor.zoneId as zonesInFloor.name for zonesInFloor in zonesInFloor]')).get(1);
	ptor.sleep(5000);
   	
    
 	
 	 element.all(by.model('searchModel.tagType')).sendKeys('Assets');
 	ptor.sleep(5000);
 	element.all(by.model('searchModel.tagType')).sendKeys('All');
 	ptor.sleep(5000);
 	element.all(by.model('searchModel.tagType')).sendKeys('Patients');
 	ptor.sleep(5000);
 	element.all(by.model('searchModel.searchText')).sendKeys('john');
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="updateAssetSearch()"]')).click();
 	ptor.sleep(5000);
 	element.all(by.model('searchModel.searchText')).clear();
 	ptor.sleep(5000);
 	element.all(by.model('searchModel.searchText')).sendKeys('miller');
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="updateAssetSearch()"]')).click();
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="clearAssetSearch()"]')).click();
 	ptor.sleep(5000);
 	var glbl=element.all(by.model('tagIdName'));
 	glbl.sendKeys('151');
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="selectMatch($index)"]')).click();
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="changeFloorTagSelection()"]')).click();
 	ptor.sleep(5000);
 	element.all(by.css('[ng-click="clearTagSearch()"]')).click();
 	ptor.sleep(5000);
 	var track=element.all(by.css('[ng-click="changeHoverRegion(listItem.tagId)"]')).get(0);
 	track.click();
 	ptor.sleep(5000);
   })
})
 describe('Test-Reports...', function() {	
    	  
         it('TESTCASE  PFA Report',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(0);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/pfa');
        	 ptor.sleep(2000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/15/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
         });
  
         describe('Test-PFA validations', function() {	
        	 
        	 it('Test case:Start Date and end date required',function(){
            	 
            	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
            	 element.all(by.model('reportSearchModel.startTime')).clear();
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	
            	 
            		element.all(by.id('generatebtn')).click();
                	 ptor.sleep(10000);
                	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/pfa');
             });
        	 
         it('Test case:Start Date can not be greater than Todays Date',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/25/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/pfa');
         });
          it('Test case:Start Date can not be greater than End Date ',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/24/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	  element.all(by.model('reportSearchModel.endTime')).clear();
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
         	 element.all(by.model('reportSearchModel.endTime')).sendKeys('12/20/2015 00:00:00');
         	 ptor.sleep(2000);
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/pfa');
         });
         });
         it('Test case OR PFA report',function(){
        	 
        	 
            	 element.all(by.css('[ng-click="resetTime()"]')).click();
        	 element.all(by.model('reportModel.deptId')).sendKeys('OR');
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(5000);
         	
         	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/15/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
         });
         it('TESTCASE: Length of stay Report',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(1);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
        	 ptor.sleep(2000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/03/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	  element.all(by.id('generatebtn')).click();
          	 ptor.sleep(10000);
        	 
          	 
          	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/15/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 
        	
         });
        describe('Test: Length of stay Report validations', function() {	
        	 
        	 it('Test case:Start Date and end date required',function(){
            	 
            	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
            	 element.all(by.model('reportSearchModel.startTime')).clear();
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	
            	 
            		element.all(by.id('generatebtn')).click();
                	 ptor.sleep(10000);
                	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
             });
        	 
         it('Test case:Start Date can not be greater than Todays Date',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/25/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
         });
          it('Test case:Start Date can not be greater than End Date ',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/24/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	  element.all(by.model('reportSearchModel.endTime')).clear();
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
         	 element.all(by.model('reportSearchModel.endTime')).sendKeys('12/20/2015 00:00:00');
         	 ptor.sleep(2000);
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
         });
         });
         it('Test case for ER Length of stay report',function(){
        	 
            	 element.all(by.css('[ng-click="resetTime()"]')).click();
            	 ptor.sleep(1000);
         	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
        	 ptor.sleep(5000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	 ptor.sleep(2000);
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(10000);
         	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
       	 ptor.sleep(5000);
       	 element.all(by.model('reportSearchModel.startTime')).clear();
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
    	  ptor.sleep(1000);
    	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/16/2015 00:00:00');
    	 ptor.sleep(2000);
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
        	element.all(by.id('generatebtn')).click();
        	 ptor.sleep(10000);
        	 element.all(by.css('[ng-click="resetTime()"]')).click();
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
         });
         it('Test case for OR Length of stay report',function(){
        	 element.all(by.model('reportModel.deptId')).sendKeys('OR');
        	 ptor.sleep(5000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/03/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(5000);
         	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
        	 ptor.sleep(5000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(10000);
         	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
       	 ptor.sleep(5000);
       	 element.all(by.model('reportSearchModel.startTime')).clear();
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
    	  ptor.sleep(1000);
    	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/14/2015 00:00:00');
    	 ptor.sleep(2000);
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
    	  ptor.sleep(1000);
        	element.all(by.id('generatebtn')).click();
        	 ptor.sleep(10000);
        	
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/los');
         });
         it('TESTCASE: Time in Procedure Report',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(2);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
        	 ptor.sleep(2000);
        	 
        	 
        	 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/15/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);

         });
      describe('Test: procedure validations', function() {	
        	 
        	 it('Test case:Start Date and end date required',function(){
            	 
            	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
            	 element.all(by.model('reportSearchModel.startTime')).clear();
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	
            	 
            		element.all(by.id('generatebtn')).click();
                	 ptor.sleep(10000);
                	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
             });
        	 
         it('Test case:Start Date can not be greater than Todays Date',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/25/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
         });
          it('Test case:Start Date can not be greater than End Date ',function(){
        	 
        	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/24/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	  element.all(by.model('reportSearchModel.endTime')).clear();
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
         	 element.all(by.model('reportSearchModel.endTime')).sendKeys('12/20/2015 00:00:00');
         	 ptor.sleep(2000);
         	 var dropdown = element.all(by.css('.dropdown')).get(0);
         	 dropdown.click();
         	  ptor.sleep(1000);
        		element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
         });
         });
         it('Test case for ER Time in Procedure Report',function(){
        	
            	 element.all(by.css('[ng-click="resetTime()"]')).click();
            	 ptor.sleep(1000);
         	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
        	 ptor.sleep(5000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(10000);
         	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
       	 ptor.sleep(5000);
       	 element.all(by.model('reportSearchModel.startTime')).clear();
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
    	  ptor.sleep(1000);
    	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/14/2015 00:00:00');
    	 ptor.sleep(2000);
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
        	element.all(by.id('generatebtn')).click();
        	 ptor.sleep(10000);
        	 element.all(by.css('[ng-click="resetTime()"]')).click();
        	  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
         });
         it('Test case for OR Time in Procedure Report',function(){
        	 element.all(by.model('reportModel.deptId')).sendKeys('OR');
           	 ptor.sleep(5000);
           	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/03/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	 element.all(by.id('generatebtn')).click();
         	 ptor.sleep(10000);
         	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
        	 ptor.sleep(5000);
         	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
         	element.all(by.id('generatebtn')).click();
         	 ptor.sleep(10000);
         	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
       	 ptor.sleep(5000);
     	 element.all(by.model('reportSearchModel.startTime')).clear();
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
    	  ptor.sleep(1000);
    	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/14/2015 00:00:00');
    	 ptor.sleep(2000);
    	 
    	 var dropdown = element.all(by.css('.dropdown')).get(0);
    	 dropdown.click();
        	element.all(by.id('generatebtn')).click();
        	 ptor.sleep(10000);
        	 element.all(by.css('[ng-click="resetTime()"]')).click();
        	  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/procedure');
         });
            it('TESTCASE: Time in ED Bed/Hallway Report',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(3);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
        	 ptor.sleep(2000);
        	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/14/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
            	element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
        	
         });
            it('Test case for ER Time in  ED Bed/Hallway',function(){
             	
            	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
           	 ptor.sleep(5000);
           	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);       	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
            	
            	element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
          	 ptor.sleep(5000);
          	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/14/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
            	element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
           
           	 element.all(by.css('[ng-click="resetTime()"]')).click();
           	  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
        
            });
            
            describe('Test:Time in  ED Bed/Hallway validations', function() {	
           	 
           	 it('Test case:Start Date and end date required',function(){
               	 
               	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
               	 element.all(by.model('reportSearchModel.startTime')).clear();
               	 var dropdown = element.all(by.css('.dropdown')).get(0);
               	 dropdown.click();
               	
               	 
               		element.all(by.id('generatebtn')).click();
                   	 ptor.sleep(10000);
                   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
                });
           	 
            it('Test case:Start Date can not be greater than Todays Date',function(){
           	 
           	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
           	 element.all(by.model('reportSearchModel.startTime')).clear();
           	 var dropdown = element.all(by.css('.dropdown')).get(0);
           	 dropdown.click();
           	  ptor.sleep(1000);
           	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/25/2015 00:00:00');
           	 ptor.sleep(2000);
           	 var dropdown = element.all(by.css('.dropdown')).get(0);
           	 dropdown.click();
           	  ptor.sleep(1000);
           	 
           		element.all(by.id('generatebtn')).click();
               	 ptor.sleep(10000);
               	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
            });
             it('Test case:Start Date can not be greater than End Date ',function(){
           	 
           	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
           	 element.all(by.model('reportSearchModel.startTime')).clear();
           	 var dropdown = element.all(by.css('.dropdown')).get(0);
           	 dropdown.click();
           	  ptor.sleep(1000);
           	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/24/2015 00:00:00');
           	 ptor.sleep(2000);
           	 var dropdown = element.all(by.css('.dropdown')).get(0);
           	 dropdown.click();
           	  ptor.sleep(1000);
           	  element.all(by.model('reportSearchModel.endTime')).clear();
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	  ptor.sleep(1000);
            	 element.all(by.model('reportSearchModel.endTime')).sendKeys('12/20/2015 00:00:00');
            	 ptor.sleep(2000);
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	  ptor.sleep(1000);
           		element.all(by.id('generatebtn')).click();
               	 ptor.sleep(10000);
               	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
            });
            });
            it('Test case for OR Time in  ED Bed/Hallway',function(){
            	
               	 element.all(by.css('[ng-click="resetTime()"]')).click();
               	 ptor.sleep(1000);
            	element.all(by.model('reportModel.deptId')).sendKeys('OR');
              	 ptor.sleep(5000);
              	 element.all(by.model('reportSearchModel.startTime')).clear();
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
            	  ptor.sleep(1000);
            	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/03/2015 00:00:00');
            	 ptor.sleep(2000);
            	 
            	 var dropdown = element.all(by.css('.dropdown')).get(0);
            	 dropdown.click();
                	element.all(by.id('generatebtn')).click();
                	 ptor.sleep(10000);
            	 element.all(by.model('reportModel.reportUnit')).sendKeys('Days');
           	 ptor.sleep(5000);
           	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
            	element.all(by.id('generatebtn')).click();
            	 ptor.sleep(10000);
            	element.all(by.model('reportModel.reportUnit')).sendKeys('Months');
          	 ptor.sleep(5000);
          	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('11/14/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
           	element.all(by.id('generatebtn')).click();
           	 ptor.sleep(10000);
           	 element.all(by.css('[ng-click="resetTime()"]')).click();
           	  expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/dashboard10/encounters/edbedhallway');
            });
              it('TESTCASE: Tag Status Report ',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(4);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/locationLogReport/tagLocationReport');
        	 ptor.sleep(2000);
        	
         });
              it('TESTCASE: Generate Tag Status Report ',function(){
             	
            	  element.all(by.model('tagId')).sendKeys('100');
            	  ptor.sleep(2000);
            	  var pat = element.all(by.css('[ng-click="selectMatch($index)"]')).get(0);
      	    	 pat.click();
      	    	 ptor.sleep(9000);
            	  element.all(by.css('[ng-click="updateSearch()"]')).click();
             	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/locationLogReport/tagLocationReport');
             	 ptor.sleep(2000);
             	
              });
              
            it('TESTCASE: Tags Lost  Report',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(5);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/tagsLostReport/inactiveTags');
        	 ptor.sleep(2000);
        	
         });
            it('TESTCASE: Tags Lost  Report',function(){
            	 element.all(by.model('reportModel.hour')).sendKeys('11');
               	 ptor.sleep(5000);
               	 element.all(by.model('reportModel.minute')).sendKeys('30');
               	 ptor.sleep(5000);
             
           	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/tagsLostReport/inactiveTags');
           	 ptor.sleep(2000);
           	
            });
            
            it('TESTCASE: Unassigned Tags  Report DropBox',function(){
        	 var dropdown = element.all(by.css('.dropdown')).get(3);
        	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
        	 toggle.click();
        	  ptor.sleep(1000);
        	  
        	  var list = dropdown.all(by.css('.dropdown-menu li'));
        	  var test= list.get(6);
        		test.click(); 
        	  ptor.sleep(1000);
          
        	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/locationLogReport/unassignedTagReport');
        	 ptor.sleep(2000);
        	
         });      
            it('TESTCASE: Unassigned Tags  Report non DropBox',function(){
            	 element.all(by.model('reportModel.zoneType')).sendKeys('NonDropBox');
               	 ptor.sleep(5000);
             
           	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/locationLogReport/unassignedTagReport');
           	 ptor.sleep(2000);
           	
            });      
      
            it('TESTCASE: Violations Report for ER',function(){
           	 var dropdown = element.all(by.css('.dropdown')).get(3);
           	 var toggle = dropdown.element(by.css('.dropdown-toggle'));
           	 toggle.click();
           	  ptor.sleep(1000);
           	  
           	  var list = dropdown.all(by.css('.dropdown-menu li'));
           	  var test= list.get(7);
           		test.click(); 
           	  ptor.sleep(1000);
             
           	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/violationReport/violationReport');
           	 ptor.sleep(2000);
           	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	 ptor.sleep(2000);
        	 element.all(by.css('[ng-click="violationReport()"]')).click();
        	 ptor.sleep(10000);
        	 
        	 
        	 
        		
            }); 
            
            describe('Test:Violations Report validations', function() {	
              	 
              	 it('Test case:Start Date and end date required',function(){
                  	 
                  	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
                  	 element.all(by.model('reportSearchModel.startTime')).clear();
                  	 var dropdown = element.all(by.css('.dropdown')).get(0);
                  	 dropdown.click();
                  	
                  	 
                  	element.all(by.css('[ng-click="violationReport()"]')).click();
                      	 ptor.sleep(10000);
                      	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/violationReport/violationReport');
                   });
              	 
               it('Test case:Start Date can not be greater than Todays Date',function(){
              	 
              	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
              	 element.all(by.model('reportSearchModel.startTime')).clear();
              	 var dropdown = element.all(by.css('.dropdown')).get(0);
              	 dropdown.click();
              	  ptor.sleep(1000);
              	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/25/2015 00:00:00');
              	 ptor.sleep(2000);
              	 var dropdown = element.all(by.css('.dropdown')).get(0);
              	 dropdown.click();
              	  ptor.sleep(1000);
              	 
              	element.all(by.css('[ng-click="violationReport()"]')).click();
                  	 ptor.sleep(10000);
                  	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/violationReport/violationReport');
               });
                it('Test case:Start Date can not be greater than End Date ',function(){
              	 
              	 element.all(by.css('[ng-click="resetTime()"]')).click(); 
              	 element.all(by.model('reportSearchModel.startTime')).clear();
              	 var dropdown = element.all(by.css('.dropdown')).get(0);
              	 dropdown.click();
              	  ptor.sleep(1000);
              	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/24/2015 00:00:00');
              	 ptor.sleep(2000);
              	 var dropdown = element.all(by.css('.dropdown')).get(0);
              	 dropdown.click();
              	  ptor.sleep(1000);
              	  element.all(by.model('reportSearchModel.endTime')).clear();
               	 var dropdown = element.all(by.css('.dropdown')).get(0);
               	 dropdown.click();
               	  ptor.sleep(1000);
               	 element.all(by.model('reportSearchModel.endTime')).sendKeys('12/20/2015 00:00:00');
               	 ptor.sleep(2000);
               	 var dropdown = element.all(by.css('.dropdown')).get(0);
               	 dropdown.click();
               	  ptor.sleep(1000);
               	element.all(by.css('[ng-click="violationReport()"]')).click();
                  	 ptor.sleep(10000);
                  	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/violationReport/violationReport');
               });
               });
            it('TESTCASE: Violations Report for OR',function(){
            	
                  	 element.all(by.css('[ng-click="resetTime()"]')).click();
                  	 ptor.sleep(1000);
            	element.all(by.model('reportModel.deptId')).sendKeys('OR');
           	 ptor.sleep(5000);
           	 element.all(by.model('reportSearchModel.startTime')).clear();
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	  ptor.sleep(1000);
        	 element.all(by.model('reportSearchModel.startTime')).sendKeys('12/16/2015 00:00:00');
        	 ptor.sleep(2000);
        	 
        	 var dropdown = element.all(by.css('.dropdown')).get(0);
        	 dropdown.click();
        	 ptor.sleep(2000);
           	 element.all(by.css('[ng-click="violationReport()"]')).click();
              	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/violationReport/violationReport');
              	 ptor.sleep(2000);
              	
               });      
            it('TEST Click Logo for show all Dashboards',function(){
            	var logo=element(by.id('logo'));
            	  logo.click();
            	  ptor.sleep(2000);
        		
        	}); 
        
		
});	
describe('Test-Audit Report', function() {	
	it('Audit Information', function (){
   	 
	  	 var Aud = element.all(by.css('.btn')).get(4);
	   	 Aud.click();
	  	 ptor.sleep(2000);
   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
   	 ptor.sleep(2000);
    });
 
        
    it('Audit Report', function(){
   	
   	 element.all(by.model('auditModel.reportOption')).sendKeys('Login/Logout');
   	element.all(by.model('searchModel.searchText')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.model('auditModel.reportOption')).sendKeys('Add');
    	 element.all(by.model('searchModel.searchText')).click();
    	 ptor.sleep(5000);
    	 
    	element.all(by.model('auditModel.reportOption')).sendKeys('Update');
   	element.all(by.model('searchModel.searchText')).click();
       ptor.sleep(5000);
   	 
   	 element.all(by.model('auditModel.reportOption')).sendKeys('All');
        element.all(by.model('searchModel.searchText')).click();
    	 ptor.sleep(5000);
    	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
    });
    
    
    it('Audit Date', function(){
   	 
   	 //Future Start Date
   	 element.all(by.model('auditSearchModel.startTime')).clear();
   	 element.all(by.model('auditSearchModel.startTime')).sendKeys('01/30/2016');
   	
   	 element.all(by.model('searchModel.searchText')).click();
   	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(3000);
   	 
   	 errMsg = element.all(by.css('.ng-binding')).get(1);
   	 expect(errMsg.getText()).toEqual( "Start Date can not be greater than Today's Date");
   	 ptor.sleep(2000);
   	 element.all(by.css('[ng-click="resetTime()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 //More than 90 Days
   	 element.all(by.model('auditSearchModel.startTime')).clear();
   	 element.all(by.model('auditSearchModel.startTime')).sendKeys('06/21/2014');
   	 
   	 element.all(by.model('searchModel.searchText')).click();
   	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(3000);
   	 
   	 errMsg = element.all(by.css('.ng-binding')).get(1);
   	 expect(errMsg.getText()).toEqual("Difference between start Date and End Date can not be more than 90 days");
   	 ptor.sleep(2000);
   	 element.all(by.css('[ng-click="resetTime()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 
   	 //Past End Date
   	 element.all(by.model('auditSearchModel.endTime')).clear();
   	 element.all(by.model('auditSearchModel.endTime')).sendKeys('06/20/2015');
   	
   	 element.all(by.model('searchModel.searchText')).click();
   	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(3000);
   	 
   	 errMsg = element.all(by.css('.ng-binding')).get(1);
   	 expect(errMsg.getText()).toEqual( "Start Date can not be greater than End Date");
   	 ptor.sleep(2000);
   	 element.all(by.css('[ng-click="resetTime()"]')).click();
   	 ptor.sleep(3000);
   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
   	
    });
    
    it('Audit Date with Login/Logout',function(){
   	
   	 element.all(by.model('auditSearchModel.startTime')).clear();
   	 element.all(by.model('auditSearchModel.startTime')).sendKeys('11/05/2015');
   	 ptor.sleep(2000);
   	 element.all(by.model('auditModel.reportOption')).sendKeys('Login/Logout');
   	 ptor.sleep(3000);
   	 
    	 element.all(by.model('searchModel.searchText')).sendKeys('security');
    	 ptor.sleep(1000);
    	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.model('searchModel.searchText')).clear();
   	 element.all(by.model('searchModel.searchText')).sendKeys('gautam');
    	 ptor.sleep(1000);
    	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.model('searchModel.searchText')).clear();
   	 element.all(by.model('searchModel.searchText')).sendKeys('nagarjuna');
    	 ptor.sleep(1000);
    	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.model('searchModel.searchText')).clear();
   	 element.all(by.model('searchModel.searchText')).sendKeys('superadmin');
    	 ptor.sleep(1000);
    	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(5000);
   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
    });
    
    it('Sorting In Asc,Desc & Expo', function (){
   	 
   	 element.all(by.model('auditModel.sortOrder')).sendKeys('Time Stamp Desc');
   	 ptor.sleep(5000);
   	 element.all(by.model('auditModel.sortOrder')).sendKeys('Time Stamp Asc');
   	 ptor.sleep(5000);
   	 
   	 element.all(by.model('auditModel.reportOption')).sendKeys('Login/Logout');
   	 ptor.sleep(5000);
   	 element.all(by.model('auditModel.sortOrder')).sendKeys('Time Stamp Desc');
   	 ptor.sleep(5000);
   	 
   	 element.all(by.css('[ng-click="exportResults()"]')).click();
   	 ptor.sleep(5000);
   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
    });
    
    it('Reports Add Appointments',function(){
   	
   	 element.all(by.model('auditModel.reportOption')).sendKeys('Add');
   	 ptor.sleep(3000);
   	 element.all(by.model('auditSearchModel.startTime')).clear();
   	 ptor.sleep(1000);
   	 element.all(by.model('auditSearchModel.startTime')).sendKeys('10/16/2015');
   	 element.all(by.css('[ng-click="auditSearch()"]')).click();
   	 ptor.sleep(5000);
   	 element.all(by.css('[ng-click="lastPage()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.css('[ng-click="exportResults()"]')).click();
   	 ptor.sleep(5000);
   	 
   	 element.all(by.css('[ng-click="resetTime()"]')).click();
   	 ptor.sleep(3000);
   	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/auditListViewTable/audit');
    	 
        // Tagnos (Home)
         	     var hom  = element.all(by.css('.navbar-header'));
         	     hom.click();
         	     ptor.sleep(1000);
         	     expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/home');
         	     ptor.sleep(1000);
   	 
    });
}); 
describe('Test-logout', function() {	
it('TEST CASE-11 : Sign Out',function(){
	  
	 var lgOut = element(by.css('.im-exit'));
	  lgOut.click();
	  ptor.sleep(2000);
	 expect(browser.getCurrentUrl()).toEqual('http://localhost:8080/phoenixmapp/app/#/');
	 ptor.sleep(2000);
}); 
});